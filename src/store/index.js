import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    marker1: 0,
    marker2: 0,
  },
  mutations: {
    setmarker1 (state, value) {
      state.count = value
    }
  },
  actions: {
  },
  modules: {
  }
})
